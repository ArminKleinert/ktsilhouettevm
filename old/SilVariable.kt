//package de.kleinert.sil
//
//import java.math.BigInteger
//import java.math.BigDecimal
//
//object SilVariableCast {
//}
//
//object SilVariables {
//
//	fun compareCategory(obj0: SilVariable, obj1: SilVariable): Int {
//		return obj0.type.compCategory.compareTo(obj1.type.compCategory)
//	}
//
//	fun compare(obj0: SilVariable, obj1: SilVariable): Int {
//		if (obj0.javaClass == obj1.javaClass) {
//			return obj0.compareTo(obj1)
//		} else {
//			return compareCategory(obj0, obj1)
//		}
//	}
//
//	fun isTrue(obj: SilVariable?): Boolean = obj != null && obj.value != false
//
//	fun intToVar(arg: Int): SilNumeric = SilNumeric(arg)
//
//	fun longToVar(arg: Long): SilNumeric = SilNumeric(arg)
//
//	fun doubleToVar(arg: Double): SilNumeric = SilNumeric(arg)
//
//	fun stringToVar(arg: Any): SilString = SilString(arg.toString())
//
//	fun varToBool(obj: SilVariable): Boolean = isTrue(obj)
//
//	// TODO
//	fun varToInt(obj: SilVariable): Int = 0
//
//	// TODO
//	fun varToLong(obj: SilVariable): Long = 0
//
//	// TODO
//	fun varToDouble(obj: SilVariable): Double = 0.0
//
//	fun varToString(obj: SilVariable): String = obj.toString()
//
//	fun varToSilString(obj: SilVariable): SilString = obj.toSilString()
//
//	fun throwFrozenModificationExceptionSV(vararg texts: Any): SilVariable =
//			throw RuntimeException("Tried to modify frozen objects: " + texts.joinToString(", "))
//
//	fun throwFrozenModificationExceptionBool(vararg texts: Any): Boolean {
//		throw RuntimeException("Tried to modify frozen objects: " + texts.joinToString(", "))
//	}
//
//	fun throwFrozenModificationExceptionUnit(vararg texts: Any): Unit {
//		throw RuntimeException("Tried to modify frozen objects: " + texts.joinToString(", "))
//	}
//
//	fun throwIllegalComparison(obj0: SilVariable, obj1: SilVariable): Unit {
//		val type0 = obj0.javaClass
//		val type1 = obj1.javaClass
//		throw RuntimeException("Illegal comparison of types: $type0 and $type1")
//	}
//}
//
//interface SilVariable : Comparable<SilVariable> {
//	public val value: Any?
//	public val frozen: Boolean
//	public val type: SilVariableType
//
//	fun freeze(): SilVariable {
//		return this
//	}
//
//	fun toSilString(): SilString = SilString(toString())
//
//	enum class SilVariableType(val compCategory: Int) : Comparable<SilVariableType> {
//		ANY(-1), BOOL(0), NUMBER(1), FUNCTION(2), STRING(3), ARRAY(4), MAP(5);
//	}
//}
//
//class SilNumeric(override val value: Number) : SilVariable, Number() {
//
//	override val frozen = true
//	override val type = SilVariable.SilVariableType.NUMBER
//
//	override fun toDouble(): Double = value.toDouble()
//
//	override fun toFloat(): Float = value.toFloat()
//
//	override fun toLong(): Long = value.toLong()
//
//	override fun toInt(): Int = value.toInt()
//
//	override fun toChar(): Char = value.toChar()
//
//	override fun toShort(): Short = value.toShort()
//
//	override fun toByte(): Byte = value.toByte()
//
//	fun toBigInt(): BigInteger {
//		return if (value is BigInteger) value else if (value is BigDecimal) value.toBigIntegerExact() else BigInteger(value.toString())
//	}
//
//	fun toBigDec(): BigDecimal {
//		return if (value is BigDecimal) value else if (value is BigInteger) value.toBigDecimal() else BigDecimal(value.toString())
//	}
//
//	fun toBytes(): ByteArray = toBigInt().toByteArray()
//
//	override fun toString(): String = value.toString()
//
//	override fun compareTo(other: SilVariable): Int {
//		if (other is SilNumeric) {
//			return toString().compareTo(other.toString())
//		}
//		SilVariables.throwIllegalComparison(this, other)
//		return 0xFFFF // unreachable
//	}
//}
//
//class SilString(override val value: String) : SilVariable, CharSequence {
//
//	override val frozen = true
//	override val type = SilVariable.SilVariableType.STRING
//
//	operator fun plus(other: Any?): SilString {
//		return if (other != null) {
//			return SilString(value + other.toString())
//		} else this
//
//	}
//
//	override val length: Int
//		get() = value.length
//
//	val size: Int
//		get() = length
//
//	override operator fun get(index: Int): Char = value.get(index)
//
//	override fun subSequence(startIndex: Int, endIndex: Int): SilString =
//			SilString(value.subSequence(startIndex, endIndex).toString())
//
//	override fun toString(): String = value.toString()
//
//	override fun compareTo(other: SilVariable): Int {
//		if (other is SilString) {
//			return toString().compareTo(other.toString())
//		}
//		SilVariables.throwIllegalComparison(this, other)
//		return 0xFFFF // unreachable
//	}
//}
//
//class SilBoolean : SilVariable {
//
//	override val value: Boolean
//
//	override val frozen = true
//	override val type = SilVariable.SilVariableType.BOOL
//
//	companion object statics {
//		val FALSE = SilBoolean(true)
//		val TRUE = SilBoolean(true)
//
//		fun getForBool(value: Boolean): SilBoolean = if (value) TRUE else FALSE
//	}
//
//	private constructor (value: Boolean) {
//		this.value = value
//	}
//
//	operator fun not(): SilBoolean = SilBoolean.getForBool(!value)
//
//	infix fun and(other: SilVariable): SilBoolean = SilBoolean.getForBool(value && SilVariables.varToBool(other))
//
//	infix fun or(other: SilVariable): SilBoolean = SilBoolean.getForBool(value || SilVariables.varToBool(other))
//
//	infix fun xor(other: SilVariable): SilBoolean = SilBoolean.getForBool(value xor SilVariables.varToBool(other))
//
//	override fun toString(): String {
//		return value.toString()
//	}
//
//	override fun compareTo(other: SilVariable): Int {
//		if (other is SilBoolean) {
//			return value.compareTo(other.value)
//		}
//		SilVariables.throwIllegalComparison(this, other)
//		return 0xFFFF // unreachable
//	}
//}
//
//class SilNull : SilVariable {
//	override val value = false
//
//	override fun toString(): String = ""
//
//	override val frozen = true
//	override val type = SilVariable.SilVariableType.BOOL
//
//	companion object inst {
//		val NULL = SilNull()
//	}
//
//	override fun compareTo(other: SilVariable): Int = if (other is SilNull) 0 else -1
//}
//
//class SilMap(override val value: MutableMap<SilVariable, SilVariable>) : SilVariable, MutableMap<SilVariable, SilVariable> {
//
//	override fun toString(): String {
//		return value.toString()
//	}
//
//	override var frozen = false
//		private set(value: Boolean) {
//			frozen = if (frozen) true else value
//		}
//
//	override val type = SilVariable.SilVariableType.MAP
//
//	override val size: Int = value.size
//
//	override fun isEmpty(): Boolean = value.isEmpty()
//
//	override fun containsKey(key: SilVariable): Boolean = value.containsKey(key)
//
//	override fun containsValue(v: SilVariable): Boolean = value.containsValue(v)
//
//	override operator fun get(key: SilVariable): SilVariable? = value.getOrDefault(key, SilNull.NULL)
//
//	override fun getOrDefault(key: SilVariable, defaultValue: SilVariable): SilVariable =
//			value.getOrDefault(key, defaultValue)
//
//	override fun put(key: SilVariable, v: SilVariable): SilVariable? =
//			if (!frozen) value.put(key, v) else SilVariables.throwFrozenModificationExceptionSV(this)
//
//	override fun remove(key: SilVariable): SilVariable? =
//			if (!frozen) value.remove(key) else SilVariables.throwFrozenModificationExceptionSV(this)
//
//	override fun remove(key: SilVariable, v: SilVariable): Boolean =
//			if (!frozen) value.remove(key, v) else SilVariables.throwFrozenModificationExceptionBool(this)
//
//	override fun putAll(from: Map<out SilVariable, SilVariable>): Unit =
//			if (!frozen) value.putAll(from) else SilVariables.throwFrozenModificationExceptionUnit(this)
//
//	override fun clear(): Unit =
//			if (!frozen) value.clear() else SilVariables.throwFrozenModificationExceptionUnit(this)
//
//	override val keys: MutableSet<SilVariable> = value.keys
//	override val values: MutableCollection<SilVariable> = value.values
//	override val entries: MutableSet<MutableMap.MutableEntry<SilVariable, SilVariable>> = value.entries
//
//	// TODO
//	override fun compareTo(other: SilVariable): Int {
//		if (other is SilMap) {
//			if (size != other.size) {
//				return size.compareTo(other.size)
//			}
//			return value.toString().compareTo(other.value.toString())
//		}
//		SilVariables.throwIllegalComparison(this, other)
//		return 0xFFFF // unreachable
//	}
//}
//
//class SilArray(override val value: MutableList<SilVariable>) : SilVariable, List<SilVariable> {
//
//	override fun toString(): String {
//		return value.toString()
//	}
//
//	override var frozen = false
//		private set(value: Boolean) {
//			frozen = if (frozen) true else value
//		}
//
//	override val type = SilVariable.SilVariableType.ARRAY
//
//	override val size: Int = value.size
//
//	override fun isEmpty(): Boolean = value.isEmpty()
//
//	override fun contains(element: @UnsafeVariance SilVariable): Boolean = value.contains(element)
//	override fun iterator(): Iterator<SilVariable> = value.iterator()
//
//	override fun containsAll(elements: Collection<@UnsafeVariance SilVariable>): Boolean = value.containsAll(elements)
//
//	override operator fun get(index: Int): SilVariable = value.get(index)
//
//	override fun indexOf(element: @UnsafeVariance SilVariable): Int = value.indexOf(element)
//
//	override fun lastIndexOf(element: @UnsafeVariance SilVariable): Int = value.lastIndexOf(element)
//
//	override fun listIterator(): ListIterator<SilVariable> = value.listIterator()
//
//	override fun listIterator(index: Int): ListIterator<SilVariable> = value.listIterator()
//
//	override fun subList(fromIndex: Int, toIndex: Int): List<SilVariable> = value.subList(fromIndex, toIndex)
//
//	fun add(element: SilVariable): Boolean = value.add(element)
//
//	fun remove(element: SilVariable): Boolean = value.remove(element)
//
//	fun addAll(elements: Collection<SilVariable>): Boolean = value.addAll(elements)
//
//	fun addAll(index: Int, elements: Collection<SilVariable>): Boolean = value.addAll(index, elements)
//
//	fun removeAll(elements: Collection<SilVariable>): Boolean = value.removeAll(elements)
//	fun retainAll(elements: Collection<SilVariable>): Boolean = value.retainAll(elements)
//	fun clear(): Unit = value.clear()
//
//	operator fun set(index: Int, element: SilVariable): SilVariable = value.set(index, element)
//
//	fun add(index: Int, element: SilVariable): Unit = value.add(index, element)
//
//	fun removeAt(index: Int): SilVariable = value.removeAt(index)
//
//	// TODO
//	override fun compareTo(other: SilVariable): Int {
//		if (other is SilArray) {
//			if (size != other.size) {
//				return size.compareTo(other.size)
//			}
//			return value.toString().compareTo(other.value.toString())
//		}
//		SilVariables.throwIllegalComparison(this, other)
//		return 0xFFFF // unreachable
//	}
//}
