//package de.kleinert.sil
//
//import java.lang.FunctionalInterface
//import java.lang.IllegalArgumentException
//
//class SilFuncArgs {
//	val iface: String
//	val args: List<SilVariable>
//
//	constructor (iface: String) {
//		this.iface = iface
//		args = mutableListOf()
//	}
//
//	constructor (vararg arguments: SilVariable) {
//		this.iface = ""
//		args = listOf(*arguments)
//	}
//
//	constructor (iface: String, vararg arguments: SilVariable) {
//		this.iface = iface
//		args = listOf(*arguments)
//	}
//
//	val size
//		get() = args.size
//}
//
//@FunctionalInterface
//interface InnerFunc {
//	operator fun invoke(args: SilFuncArgs): SilVariable
//}
//
//class SilFunction : SilVariable {
//
//	override val value: InnerFunc
//	val expectedOutputType: SilVariable.SilVariableType
//	val iface: String
//	val name: String
//	val numArgs: Int
//	val isMacro: Boolean
//	val isModifiesInnerState: Boolean
//
//	override fun toString(): String {
//		return "Function(\"iface\"\$\"$name\")"
//	}
//
//	override var frozen = true
//		private set(value: Boolean) {
//			frozen = if (frozen) true else value
//		}
//
//	override val type = SilVariable.SilVariableType.FUNCTION
//
//	override fun toSilString(): SilString = SilString(toString())
//
//	// Default run function
//
//	operator fun invoke(args: SilFuncArgs): SilVariable {
//		if (args.size != numArgs) {
//			throw IllegalArgumentException("Wrong number of arguments (expected " + numArgs + ", received " + args.size)
//		}
//		return value(args)
//	}
//
//	override fun compareTo(other: SilVariable): Int {
//		if (other is SilFunction) {
//			return toString().compareTo(other.toString())
//		}
//		SilVariables.throwIllegalComparison(this, other)
//		return 0xFFFF
//	}
//
//	// Constructor is private, use the factory companion object instead!
//
//	private constructor(
//			func: InnerFunc, expectedOutputType: SilVariable.SilVariableType, iface: String, name: String,
//			numArgs: Int, isMacro: Boolean, isModifiesInnerState: Boolean) {
//		this.value = func
//		this.expectedOutputType = expectedOutputType
//		this.iface = iface
//		this.name = name
//		this.numArgs = numArgs
//		this.isMacro = isMacro
//		this.isModifiesInnerState = isModifiesInnerState
//	}
//
//	// Factory methods to make constructing a function a little bit easier
//
//	companion object factory {
//
//		fun create(func: InnerFunc,
//				   expectedOutputType: SilVariable.SilVariableType = SilVariable.SilVariableType.ANY,
//				   numArgs: Int = -1,
//				   iface: String = "",
//				   name: String = "",
//				   isMacro: Boolean = false,
//				   isModifiesInnerState: Boolean = false): SilFunction {
//			return SilFunction(func, expectedOutputType, iface, name, numArgs, isMacro, isModifiesInnerState)
//		}
//
//		fun createBasic(numArgs: Int, func: InnerFunc): SilFunction {
//			return create(func, numArgs = numArgs)
//		}
//
//		fun createNamed(iface: String, name: String, numArgs: Int, func: InnerFunc): SilFunction {
//			return create(func, numArgs = numArgs,iface = iface, name = name)
//		}
//
//		fun createMacro(numArgs: Int, func: InnerFunc): SilFunction {
//			return create(func, numArgs = numArgs, isMacro = true)
//		}
//
//		fun createNamedMacro(iface: String, name: String, numArgs: Int, func: InnerFunc): SilFunction {
//			return create(func, numArgs = numArgs,iface = iface, name = name, isMacro = true)
//		}
//
//		fun createModifying(numArgs: Int, func: InnerFunc): SilFunction {
//			return create(func, numArgs = numArgs, isModifiesInnerState = true)
//		}
//
//		fun createNamedModifying(iface: String, name: String, numArgs: Int, func: InnerFunc): SilFunction {
//			return create(func, numArgs = numArgs, iface = iface,name = name, isModifiesInnerState = true)
//		}
//
//
//		fun createTyped(expectedOutputType: SilVariable.SilVariableType, numArgs: Int, func: InnerFunc): SilFunction {
//			return create(func, expectedOutputType, numArgs = numArgs)
//		}
//
//		fun createNamedTyped(iface: String, name: String, expectedOutputType: SilVariable.SilVariableType,
//							 numArgs: Int, func: InnerFunc): SilFunction {
//			return create(func, expectedOutputType, numArgs = numArgs,iface = iface, name = name)
//		}
//
//		fun createTypedMacro(expectedOutputType: SilVariable.SilVariableType, numArgs: Int,
//							 func: InnerFunc): SilFunction {
//			return create(func, expectedOutputType, numArgs = numArgs, isMacro = true)
//		}
//
//		fun createNamedTypedMacro(iface: String, name: String, expectedOutputType: SilVariable.SilVariableType,
//								  numArgs: Int, func: InnerFunc): SilFunction {
//			return create(func, expectedOutputType, numArgs = numArgs, iface = iface,name = name, isMacro = true)
//		}
//
//		fun createTypedModifying(expectedOutputType: SilVariable.SilVariableType, numArgs: Int, func: InnerFunc): SilFunction {
//			return create(func, expectedOutputType, numArgs = numArgs, isModifiesInnerState = true)
//		}
//
//		fun createNamedTypedModifying(iface: String, name: String, expectedOutputType: SilVariable.SilVariableType, numArgs: Int, func: InnerFunc): SilFunction {
//			return create(func, expectedOutputType, numArgs = numArgs, iface = iface, name = name, isModifiesInnerState = true)
//		}
//	}
//}
//
///**
// *
// */
//object FunctionConverter {
//
//// Returns nothing
//
//	fun toConsumer(silFunc: SilFunction): java.util.function.Consumer<SilVariable> {
//		return java.util.function.Consumer() { arg0 -> silFunc(SilFuncArgs(arg0)) }
//	}
//
//	fun toIntConsumer(silFunc: SilFunction): java.util.function.IntConsumer {
//		return java.util.function.IntConsumer { arg0 -> silFunc(SilFuncArgs(SilVariables.intToVar(arg0))) }
//	}
//
//	fun toObjIntConsumer(silFunc: SilFunction): java.util.function.ObjIntConsumer<SilVariable> {
//		return java.util.function.ObjIntConsumer { arg0, arg1 -> silFunc(SilFuncArgs(arg0, SilVariables.intToVar(arg1))) }
//	}
//
//	fun toLongConsumer(silFunc: SilFunction): java.util.function.LongConsumer {
//		return java.util.function.LongConsumer { arg0 -> silFunc(SilFuncArgs(SilVariables.longToVar(arg0))) }
//	}
//
//	fun toObjLongConsumer(silFunc: SilFunction): java.util.function.ObjLongConsumer<SilVariable> {
//		return java.util.function.ObjLongConsumer { arg0, arg1 -> silFunc(SilFuncArgs(arg0, SilVariables.longToVar(arg1))) }
//	}
//
//	fun toDoubleConsumer(silFunc: SilFunction): java.util.function.DoubleConsumer {
//		return java.util.function.DoubleConsumer { arg0 -> silFunc(SilFuncArgs(SilVariables.doubleToVar(arg0))) }
//	}
//
//	fun toObjDoubleConsumer(silFunc: SilFunction): java.util.function.ObjDoubleConsumer<SilVariable> {
//		return java.util.function.ObjDoubleConsumer { arg0, arg1 -> silFunc(SilFuncArgs(arg0, SilVariables.doubleToVar(arg1))) }
//	}
//
//// Returns Boolean
//
//	fun toBooleanSupplier(silFunc: SilFunction): java.util.function.BooleanSupplier {
//		return java.util.function.BooleanSupplier { SilVariables.varToBool(silFunc(SilFuncArgs())) }
//	}
//
//	fun toPredicate(silFunc: SilFunction): java.util.function.Predicate<SilVariable> {
//		return java.util.function.Predicate { arg0 -> SilVariables.varToBool(silFunc(SilFuncArgs(arg0))) }
//	}
//
//	fun toBiPredicate(silFunc: SilFunction): java.util.function.BiPredicate<SilVariable, SilVariable> {
//		return java.util.function.BiPredicate { arg0, arg1 -> SilVariables.varToBool(silFunc(SilFuncArgs(arg0, arg1))) }
//	}
//
//	fun toIntPredicate(silFunc: SilFunction): java.util.function.IntPredicate {
//		return java.util.function.IntPredicate { arg0 -> SilVariables.varToBool(silFunc(SilFuncArgs(SilVariables.intToVar(arg0)))) }
//	}
//
//	fun toLongPredicate(silFunc: SilFunction): java.util.function.LongPredicate {
//		return java.util.function.LongPredicate { arg0 -> SilVariables.varToBool(silFunc(SilFuncArgs(SilVariables.longToVar(arg0)))) }
//	}
//
//	fun toDoublePredicate(silFunc: SilFunction): java.util.function.DoublePredicate {
//		return java.util.function.DoublePredicate { arg0 -> SilVariables.varToBool(silFunc(SilFuncArgs(SilVariables.doubleToVar(arg0)))) }
//	}
//
//// Returns SilVariable
//
//	fun toIntFunction(silFunc: SilFunction): java.util.function.IntFunction<SilVariable> {
//		return java.util.function.IntFunction { arg0 -> silFunc(SilFuncArgs(SilVariables.intToVar(arg0))) }
//	}
//
//	fun toLongFunction(silFunc: SilFunction): java.util.function.LongFunction<SilVariable> {
//		return java.util.function.LongFunction { arg0 -> silFunc(SilFuncArgs(SilVariables.longToVar(arg0))) }
//	}
//
//	fun toDoubleFunction(silFunc: SilFunction): java.util.function.DoubleFunction<SilVariable> {
//		return java.util.function.DoubleFunction { arg0 -> silFunc(SilFuncArgs(SilVariables.doubleToVar(arg0))) }
//	}
//
//// Returns Int
//
//	fun toIntSupplier(silFunc: SilFunction): java.util.function.IntSupplier {
//		return java.util.function.IntSupplier { SilVariables.varToInt(silFunc(SilFuncArgs())) }
//	}
//
//	fun toToIntFunction(silFunc: SilFunction): java.util.function.ToIntFunction<SilVariable> {
//		return java.util.function.ToIntFunction { arg0 -> SilVariables.varToInt(silFunc(SilFuncArgs(arg0))) }
//	}
//
//	fun toToIntBiFunction(silFunc: SilFunction): java.util.function.ToIntBiFunction<SilVariable, SilVariable> {
//		return java.util.function.ToIntBiFunction { arg0, arg1 -> SilVariables.varToInt(silFunc(SilFuncArgs(arg0, arg1))) }
//	}
//
//	fun toIntUnaryOperator(silFunc: SilFunction): java.util.function.IntUnaryOperator {
//		return java.util.function.IntUnaryOperator { arg0 -> SilVariables.varToInt(silFunc(SilFuncArgs(SilVariables.intToVar(arg0)))) }
//	}
//
//	fun toIntBinaryOperator(silFunc: SilFunction): java.util.function.IntBinaryOperator {
//		return java.util.function.IntBinaryOperator { arg0, arg1 -> SilVariables.varToInt(silFunc(SilFuncArgs(SilVariables.intToVar(arg0), SilVariables.intToVar(arg1)))) }
//	}
//
//// Returns Long
//
//	fun toLongSupplier(silFunc: SilFunction): java.util.function.LongSupplier {
//		return java.util.function.LongSupplier { SilVariables.varToLong(silFunc(SilFuncArgs())) }
//	}
//
//	fun toToLongFunction(silFunc: SilFunction): java.util.function.ToLongFunction<SilVariable> {
//		return java.util.function.ToLongFunction { arg0 -> SilVariables.varToLong(silFunc(SilFuncArgs(arg0))) }
//	}
//
//	fun toToLongBiFunction(silFunc: SilFunction): java.util.function.ToLongBiFunction<SilVariable, SilVariable> {
//		return java.util.function.ToLongBiFunction { arg0, arg1 -> SilVariables.varToLong(silFunc(SilFuncArgs(arg0, arg1))) }
//	}
//
//	fun toLongUnaryOperator(silFunc: SilFunction): java.util.function.LongUnaryOperator {
//		return java.util.function.LongUnaryOperator { arg0 -> SilVariables.varToLong(silFunc(SilFuncArgs(SilVariables.longToVar(arg0)))) }
//	}
//
//	fun toLongBinaryOperator(silFunc: SilFunction): java.util.function.LongBinaryOperator {
//		return java.util.function.LongBinaryOperator { arg0, arg1 -> SilVariables.varToLong(silFunc(SilFuncArgs(SilVariables.longToVar(arg0), SilVariables.longToVar(arg1)))) }
//	}
//
//// Returns Double
//
//	fun toDoubleSupplier(silFunc: SilFunction): java.util.function.DoubleSupplier {
//		return java.util.function.DoubleSupplier { SilVariables.varToDouble(silFunc(SilFuncArgs())) }
//	}
//
//	fun toToDoubleFunction(silFunc: SilFunction): java.util.function.ToDoubleFunction<SilVariable> {
//		return java.util.function.ToDoubleFunction { arg0 -> SilVariables.varToDouble(silFunc(SilFuncArgs(arg0))) }
//	}
//
//	fun toToDoubleBiFunction(silFunc: SilFunction): java.util.function.ToDoubleBiFunction<SilVariable, SilVariable> {
//		return java.util.function.ToDoubleBiFunction { arg0, arg1 -> SilVariables.varToDouble(silFunc(SilFuncArgs(arg0, arg1))) }
//	}
//
//	fun toDoubleUnaryOperator(silFunc: SilFunction): java.util.function.DoubleUnaryOperator {
//		return java.util.function.DoubleUnaryOperator { arg0 -> SilVariables.varToDouble(silFunc(SilFuncArgs(SilVariables.doubleToVar(arg0)))) }
//	}
//
//	fun toDoubleBinaryOperator(silFunc: SilFunction): java.util.function.DoubleBinaryOperator {
//		return java.util.function.DoubleBinaryOperator { arg0, arg1 -> SilVariables.varToDouble(silFunc(SilFuncArgs(SilVariables.doubleToVar(arg0), SilVariables.doubleToVar(arg1)))) }
//	}
//
//// To other function types
//
//	fun toUnaryOperator(silFunc: SilFunction): java.util.function.UnaryOperator<SilVariable> {
//		return java.util.function.UnaryOperator() { arg0 -> silFunc(SilFuncArgs(arg0)) }
//	}
//
//	fun toBinaryOperator(silFunc: SilFunction): java.util.function.BinaryOperator<SilVariable> {
//		return java.util.function.BinaryOperator() { arg0, arg1 -> silFunc(SilFuncArgs(arg0, arg1)) }
//	}
//
//	fun toJavaFunction(silFunc: SilFunction): java.util.function.Function<SilVariable, SilVariable> {
//		return java.util.function.Function() { arg0 -> silFunc(SilFuncArgs(arg0)) }
//	}
//
//	fun toSupplier(silFunc: SilFunction): java.util.function.Supplier<SilVariable> {
//		return java.util.function.Supplier() { silFunc(SilFuncArgs()) }
//	}
//
//	fun toCallable(silFunc: SilFunction): java.util.concurrent.Callable<SilVariable> {
//		return java.util.concurrent.Callable() { silFunc(SilFuncArgs()) }
//	}
//
//	fun toBiFunction(silFunc: SilFunction): java.util.function.BiFunction<SilVariable, SilVariable, SilVariable> {
//		return java.util.function.BiFunction() { arg0, arg1 -> silFunc(SilFuncArgs(arg0, arg1)) }
//	}
//
//	fun toRunnable(silFunc: SilFunction): Runnable {
//		return Runnable() { silFunc(SilFuncArgs()) }
//	}
//}
