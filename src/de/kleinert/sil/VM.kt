package de.kleinert.sil

import de.kleinert.sil.types.SilNull

typealias Register = Int
typealias JmpLabel = String

object VM {

    const val consumerReg: Register = 0
    const val returnReg: Register = 1
    const val readReg: Register = 2
    const val errorReg: Register = 3
    //const val codeLogReg: Register = 4
    const val firstLegalReg: Register = 5

    private var _namespaces = mutableMapOf<String, SilNamespace>("std" to Std)
    private var _registers = mutableListOf<Any>()
    var codeLog: CodeLog? = null

    val registers: List<Any>
        get() = _registers.toList()

    val namespaces
        get() = _namespaces.toMap()

    @Suppress("NOTHING_TO_INLINE")
    private inline fun isRegisterOutOfBounds(reg: Register): Boolean {
        return reg >= _registers.size
    }

    private fun setRegister(dest: Register, value: Any) {
        if (dest > consumerReg) {
            while (isRegisterOutOfBounds(dest)) {
                _registers.add(SilNull)
            }
            _registers[dest] = value
        }
    }

    private fun clearRegister(reg: Register) {
        if (!isRegisterOutOfBounds(reg)) {
            _registers[reg] = SilNull
        }
    }

    fun getRegister(reg: Register): Any {
        return if (reg <= consumerReg || isRegisterOutOfBounds(reg)) {
            SilNull
        } else {
            _registers[reg]
        }
    }

    fun vm_clear_special_regs() {
        clearRegister(readReg)
        clearRegister(returnReg)
        clearRegister(errorReg)
    }

    fun vm_move(dest: Register, src: Register) {
        setRegister(dest, getRegister(src))
    }

    fun vm_move_val(dest: Register, value: Any?) {
        setRegister(dest, value ?: SilNull)
    }

    fun vm_move_int(dest: Register, value: Any?) {
        if (value is Number) {
            setRegister(dest, value.toLong())
        } else {
            setRegister(dest, value.toString().toIntOrNull() ?: 0)
        }
    }

    fun vm_move_fp(dest: Register, value: Any?) {
        if (value is Number) {
            setRegister(dest, value.toDouble())
        } else {
            setRegister(dest, value.toString().toDoubleOrNull() ?: 0.0)
        }
    }

    fun vm_set_return(value: Any?) {
        setRegister(returnReg, value ?: SilNull)
    }

    fun vm_clear_reg(reg: Register) {
        clearRegister(reg)
    }

    fun vm_run(ns: String, func: String, dest: Register, vararg args: Register) {
        val foundNs = _namespaces[ns]
        if (foundNs == null) {
            Std.error(this, dest, arrayOf(ns, func, ""))
        } else {
            val resolvedArgs = Array<Any>(args.size) { index ->
                getRegister(args[index])
            }
            foundNs(this, func, dest, resolvedArgs)
        }
    }

    fun vm_jmp(label: String) {
        codeLog?.seekLabel(label.trim())
    }

    fun vm_if_jmp(pred: Register, label: String) {
        val value = getRegister(pred)
        if (value != SilNull && value != 0 && value != false) {
            vm_jmp(label)
        }
    }

    fun vm_jmp_reg(lblReg: Register) {
        codeLog?.seekLabel(getRegister(lblReg).toString())
    }

    fun vm_if_jmp_reg(pred: Register, lblReg: Register) {
        val value = getRegister(pred)
        if (value != SilNull && value != 0 && value != false) {
            vm_jmp_reg(lblReg)
        }
    }
}
