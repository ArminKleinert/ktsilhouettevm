package de.kleinert.sil

import de.kleinert.sil.types.SilNull

typealias SilFunction = (VM, dest: Register, Array<Any>) -> Any

interface SilNamespace {
    val name: String
    val functions: MutableMap<String, SilFunction>

    operator fun invoke(vm: VM, funName: String, dest: Register, args: Array<Any>): Any {
        val func = functions[funName]
        return if (func == null) SilNull else func(vm, dest, args)
    }
}

object Std : SilNamespace {
    override val name = "std"
    override val functions = mutableMapOf<String, SilFunction>()

    val toInt: SilFunction = { vm, dest, args ->
        val value: Int = args[0].toString().toInt()
        vm.vm_move_int(dest, value)
        vm.vm_set_return(value)
    }

    val printStr: SilFunction = { vm, _, args ->
        print(args[0])
        vm.vm_set_return(args[0])
    }

    val printStrLn: SilFunction = { vm, _, args ->
        println(args[0])
        vm.vm_set_return(args[0])
    }

    val readln: SilFunction = { vm, _, _ ->
        val value = readLine() ?: ""
        vm.vm_move_val(VM.readReg, value)
        vm.vm_set_return(value)
    }

    val error: SilFunction = { vm, _, args ->
        args[0] = "Error in namespace ${args[0]}, function ${args[1]}: " + args[2]
        vm.vm_move_val(VM.errorReg, args[0])
        System.err.println(args[0])
    }

    val newline: SilFunction = { _, _, _ ->
        println()
    }

    val newString: SilFunction = { vm, dest, args ->
        vm.vm_set_return(args.joinToString(separator = " "))
        vm.vm_move(dest, VM.returnReg)
    }

    val newArray: SilFunction = { vm, dest, args ->
        vm.vm_set_return(Parser.arrayOfAnyToIntArray(restOf(args)))
        vm.vm_move(dest, VM.returnReg)
    }

    val increment: SilFunction = { vm, dest, args ->
        if (args[0] is Number) {
            vm.vm_set_return((args[0] as Number).toInt() + 1)
            vm.vm_move(dest, VM.returnReg)
        }
    }

    val lessThan: SilFunction = { vm, dest, args ->
        if (args[0] is Number && args[1] is Number) {
            vm.vm_set_return((args[0] as Number).toDouble() < (args[1] as Number).toDouble())
            vm.vm_move(dest, VM.returnReg)
        }
    }

    init {
        functions["print"] = printStr
        functions["println"] = printStrLn
        functions["readln"] = readln
        functions["error"] = error
        functions["newline"] = newline
        functions["string"] = newString
        functions["array"] = newArray
        functions["inc"] = increment
        functions["lt"] = lessThan
    }

    private fun <T> restOf(coll: Array<T>): List<T> {
        return coll.slice(1..(coll.size - 1))
    }

    private fun reg(anyVal: Any): Register {
        return Parser.stringToReg(anyVal)
    }
}
