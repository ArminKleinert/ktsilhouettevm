package de.kleinert.sil

import java.io.File
import java.io.IOException
import java.io.RandomAccessFile

class CodeLog(_logFile: String) {

    private val linebreak = System.getProperty("line.separator")
    private val _jmpLabels: MutableMap<String, Long> = mutableMapOf()

    val jmpLabels
        get() = _jmpLabels.toMap()

    val logFile: File = File(_logFile)
    var logFileReader: RandomAccessFile
        private set

    init {
        if (!logFile.exists()) {
            logFile.createNewFile()
        }
        logFileReader = RandomAccessFile(logFile, "r")
    }

    fun hasLabel(label: String): Boolean {
        return _jmpLabels[label.trim()] != null
    }

    fun addLabel(label: String) {
        _jmpLabels[label.trim()] = logFileReader.filePointer
    }

    fun nextLine(): String {
        return logFileReader.readLine() ?: ""
    }

    fun seekLabel(_label: String) {
        val label = _label.trim()
        val labelPos = _jmpLabels[label]
        if (labelPos != null) {
            // The label is known
            logFileReader.seek(labelPos)
        } else {
            val oldPos = logFileReader.filePointer
            var tempLine: String? = logFileReader.readLine()
            while (tempLine != null && tempLine.trim() != label) {
                tempLine = logFileReader.readLine()
            }
            if (tempLine != null) {
                // The label was found further into the code
                addLabel(label)
            } else {
                // The label could not be found
                logFileReader.seek(oldPos)
            }
        }
    }

    fun close() {
        try {
            logFileReader.close()
        } catch (ex: IOException) {
        }
    }

    fun isEOF(): Boolean {
        val mark = logFileReader.filePointer
        val res = logFileReader.read() == -1
        logFileReader.seek(mark)
        return res
    }
}
