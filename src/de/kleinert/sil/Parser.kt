package de.kleinert.sil

object Parser {

    val mnemonics: List<String> = listOf("move",
            "move_val",
            "move_int",
            "move_fp",
            "set_return",
            "clear",
            "run",
            "clear_special_regs",
            "jmp",
            "if_jmp",
            "jmp_reg",
            "if_jmp_reg")

    fun stringToReg(regString: Any): Register {
        return regString.toString().toInt()
    }

    fun arrayOfAnyToIntArray(anyArray: Collection<Any>): IntArray {
        val l = IntArray(anyArray.size)
        anyArray.mapIndexed { i, e -> l[i] = e.toString().toInt() }
        return l
    }

    fun execCommand(cmdString: String, dest: Register, vararg args: Any) {
        execCommand(mnemonics.indexOf(cmdString), dest, *args)
    }

    fun execCommand(cmdNumber: Int, dest: Register, vararg args: Any) {
        when (cmdNumber) {
            0 -> VM.vm_move(dest, reg(args[0]))
            1 -> VM.vm_move_val(dest, args[0])
            2 -> VM.vm_move_int(dest, args[0])
            3 -> VM.vm_move_fp(dest, args[0])
            4 -> VM.vm_set_return(args[0])
            5 -> VM.vm_clear_reg(dest)
            6 -> vmRunSubCommand(dest, args)
            7 -> VM.vm_clear_special_regs()
            8 -> VM.vm_jmp(args[0].toString())
            9 -> VM.vm_if_jmp(reg(args[0]), args[1].toString())
            10 -> VM.vm_jmp_reg(reg(args[0]))
            10 -> VM.vm_if_jmp_reg(reg(args[0]), reg(args[1]))
        }
    }

    fun parseLine(line: String) {
        if (line.trim().isEmpty()) {
            return
        }

        var parts = line.trim().split(" ")

        val cl = VM.codeLog
        if (cl != null && !cl.isEOF()) {
            if (isLabel(parts[0]) && !cl.hasLabel(parts[0])) {
                VM.codeLog?.addLabel(line)
                return
            }
        }

        if (!mnemonics.contains(parts[0])) {
            return
        }

        if (parts.size == 1) {
            parts = mutableListOf(parts[0], "_")
        }

        val dest = if (parts[1] == "_") {
            VM.consumerReg
        } else {
            parts[1].toInt()
        }
        val args: Array<Any> = parts.subList(2, parts.size).toTypedArray()
        execCommand(parts[0], dest, *args)
    }

    fun parseFile(filePath: String) {
        VM.codeLog = CodeLog(filePath)
        while (!VM.codeLog!!.isEOF()) {
            val line = VM.codeLog!!.nextLine()
            Parser.parseLine(line)
        }
        VM.codeLog?.close()
    }

    private fun vmRunSubCommand(dest: Register, args: Array<out Any>) {
        val arr = if (args.size > 2) {
            arrayOfAnyToIntArray(args.slice(2..(args.size - 1)))
        } else {
            IntArray(0)
        }
        VM.vm_run(args[0].toString(), args[1].toString(), dest, *arr)
    }

    private fun isLabel(str: String): Boolean {
        return str.startsWith(":") && str.length > 2 && str.endsWith(":")
    }

    private fun reg(anyVal: Any): Register {
        return if (anyVal is Number) anyVal.toInt() else anyVal.toString().toInt()
    }
}

fun main(args: Array<String>) {
    Parser.parseFile("./testcode.sil")
}
